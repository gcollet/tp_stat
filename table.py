table = {
    5 : { 5 :  2,  6 :  3,  7 :  5,  8 :  6,  9 :  7, 10 :  8, 11 :  9, 12 : 11, 13 : 12, 14 : 13, 15 : 14, 16 : 15},
    6 : { 6 :  5,  7 :  6,  8 :  8,  9 : 10, 10 : 11, 11 : 13, 12 : 14, 13 : 16, 14 : 17, 15 : 19, 16 : 21},
    7 : { 7 :  8,  8 : 10,  9 : 12, 10 : 14, 11 : 16, 12 : 18, 13 : 20, 14 : 22, 15 : 24, 16 : 26},
    8 : { 8 : 13,  9 : 15, 10 : 17, 11 : 19, 12 : 22, 13 : 24, 14 : 26, 15 : 29, 16 : 31},
    9 : { 9 : 17, 10 : 20, 11 : 23, 12 : 26, 13 : 28, 14 : 31, 15 : 34, 16 : 37},
    10 : {10 : 23, 11 : 26, 12 : 29, 13 : 33, 14 : 36, 15 : 39, 16 : 42},
    11 : {11 : 30, 12 : 33, 13 : 37, 14 : 40, 15 : 44, 16 : 47},
    12 : {12 : 37, 13 : 41, 14 : 45, 15 : 49, 16 : 53},
    13 : {13 : 45, 14 : 50, 15 : 54, 16 : 59},
    14 : {14 : 55, 15 : 59, 16 : 64},
    15 : {15 : 64, 16 : 70},
    16 : {16 : 75},
}
